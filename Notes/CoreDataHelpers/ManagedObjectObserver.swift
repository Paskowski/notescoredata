//
//  ManagedObjectObserver.swift
//  notifications
//
//  Created by Stanisław Paśkowski on 15.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import CoreData

final class ManagedObjectObserver {
    
    enum ChangeType {
        case delete
        case update
    }
    
    private var token: NSObjectProtocol!
    
    init?(object: NSManagedObject, changeHandler: @escaping (ChangeType) -> ()) {
        guard let moc = object.managedObjectContext else { return nil }
        token = moc.addObjectsDidChangeNotificationObserver {
            [weak self] notification in
            guard let changeType = self?.changeType(of: object, in: notification)
            else { return }
            changeHandler(changeType)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(token)
    }
    
    private func changeType(of object: NSManagedObject, in notification: ObjectsDidChangeNotification) -> ChangeType? {
        let deleted = notification.deletedObjects.union(notification.invalidatedObjects)
        if notification.invalidatedAllObjects || deleted.containsObjectIdentical(to: object) {
            return .delete
        }
        let updated = notification.updatedObjects.union(notification.refreshedObjects)
        if updated.containsObjectIdentical(to: object) {
            return .update
        }
        return nil
    }
    
}
