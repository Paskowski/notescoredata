//
//  NSManagedObjectContext+Observers.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 22.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation
import CoreData

struct ObjectsDidChangeNotification {
    
    private let notification: Notification
    
    var insertedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInsertedObjectsKey)
    }
    
    var updatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSUpdatedObjectsKey)
    }
    
    var deletedObjects: Set<NSManagedObject> {
        return objects(forKey: NSDeletedObjectsKey)
    }
    
    var refreshedObjects: Set<NSManagedObject> {
        return objects(forKey: NSRefreshedObjectsKey)
    }
    
    var invalidatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInvalidatedObjectsKey)
    }
    
    var invalidatedAllObjects: Bool {
        return (notification as NSNotification).userInfo?[NSInvalidatedAllObjectsKey] != nil
    }
    
    var managedObjectContext: NSManagedObjectContext {
        guard let ctx = notification.object as? NSManagedObjectContext else
        { fatalError("Invalid notification object") }
        return ctx
    }
    
    init(notification: Notification) {
        assert(notification.name == NSNotification.Name.NSManagedObjectContextObjectsDidChange)
        self.notification = notification
    }
    
    private func objects(forKey key: String) -> Set<NSManagedObject>
    {
        return ((notification as NSNotification).userInfo?[key] as? Set<NSManagedObject>) ?? Set()
    }
    
}

extension NSManagedObjectContext {
    
    func addObjectsDidChangeNotificationObserver(_ handler: @escaping (ObjectsDidChangeNotification) -> ()) -> NSObjectProtocol {
        let nc = NotificationCenter.default
        return nc.addObserver(forName: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: self, queue: nil) { notification in
            let wrappedNotification = ObjectsDidChangeNotification(notification: notification)
            handler(wrappedNotification)
        }
    }
    
}
