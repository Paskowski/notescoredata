//
//  Utillities.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 22.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import Foundation

extension Sequence where Iterator.Element: AnyObject {
    func containsObjectIdentical(to object: AnyObject) -> Bool {
        return contains { $0 === object }
    }
}
