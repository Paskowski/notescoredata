//
//  MoodDetailViewController.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 15.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {
    
    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteDateLabel: UILabel!
    
    private var observer: ManagedObjectObserver?
    
    var note: Note! {
        didSet {
            print("set note")
            observer = ManagedObjectObserver(object: note) { [weak self] type in
                guard type == .delete else { return }
                _ = self?.navigationController?.popViewController(animated: true)
            }
            if isViewLoaded {
                updateViews()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViews()
    }
    
    private func updateViews() {
        noteTitleLabel.text = note.title
        noteDateLabel.text = format(date: note.date)
        navigationItem.title = note.title
    }
    
    @IBAction func deleteNote(_ sender: UIBarButtonItem) {
        note.managedObjectContext?.performChanges {
            self.note.managedObjectContext?.delete(self.note)
        }
    }
    
}

private func format(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM-dd-yyyy"
    let dateString = dateFormatter.string(from: date)
    return dateString
}
