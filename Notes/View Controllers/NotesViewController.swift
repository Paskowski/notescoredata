//
//  RootViewController.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 06.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import CoreData

class NotesViewController: UIViewController, SegueHandler {
    
    enum SegueIdentifier: String {
        case showNoteDetail = "showNoteDetail"
    }
    
    var managedObjectContext: NSManagedObjectContext!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
        case .showNoteDetail:
            guard let vc = segue.destination as? NoteDetailViewController
                else { fatalError("Wrong view controller type") }
            guard let note = dataSource.selectedObject
                else { fatalError("Showing detail, but no selected row?")}
            vc.note = note
        }
    }
    
    private var dataSource: TableViewDataSource<NotesViewController>!
    
    func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        let request = Note.sortedFetchRequest
        request.fetchBatchSize  = 20
        request.returnsObjectsAsFaults = false

        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        dataSource = TableViewDataSource(tableView: tableView, cellIdentifier: "NoteCell", fetchedResultsController: frc, delegate: self)
    }
    
    
    @IBAction func addNoteButtonPressed(_ sender: Any) {
     
        let alertController = UIAlertController(title: "Add new note", message: nil, preferredStyle: .alert)
        alertController.addTextField { (textfield) in
            textfield.keyboardType = .default
        }
        let addNoteAction = UIAlertAction(title: "Save", style: .default) { [unowned self] action in
            guard let textField = alertController.textFields?.first, let title = textField.text else {
                return
            }
            self.managedObjectContext.performChanges {
                _ = Note.insert(into: self.managedObjectContext, title: title)
            }
        }
        
        alertController.addAction(addNoteAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
}

extension NotesViewController: TableViewDataSourceDelegate {
    func configure(_ cell: NoteTableViewCell, for object: Note) {
        cell.configure(for: object)
    }
}
