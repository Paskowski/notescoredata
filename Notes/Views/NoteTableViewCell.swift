//
//  NoteTableViewCell.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 14.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

extension NoteTableViewCell {
    func configure(for note: Note) {
        titleLabel.text = note.title
    }
}
