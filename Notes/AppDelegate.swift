//
//  AppDelegate.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 06.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var persistentContainer: NSPersistentContainer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        createNotesContainer { container in
            self.persistentContainer = container
            let storyboard = self.window?.rootViewController?.storyboard
            guard let nc = storyboard?.instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController,
                let vc = nc.viewControllers.first as? NotesViewController
            else { fatalError("wrong view controller type") }
            vc.managedObjectContext = container.viewContext
            print(vc.managedObjectContext)
            self.window?.rootViewController = nc
        }
        return true
    }
}

