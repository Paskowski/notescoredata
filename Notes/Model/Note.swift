//
//  Note.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 06.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import CoreData

final class Note: NSManagedObject {
    
    @NSManaged fileprivate(set) var date: Date
    @NSManaged fileprivate(set) var title: String
    @NSManaged fileprivate(set) var content: String
    
    static func insert(into context: NSManagedObjectContext, title: String) -> Note {
        let note: Note = context.insertObject()
        note.title = title
        note.date = Date()
        return note
    }
    
}

extension Note: Managed {
    
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: #keyPath(date), ascending: false)]
    }
    
}
