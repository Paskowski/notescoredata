//
//  NotesStack.swift
//  Notes
//
//  Created by Stanisław Paśkowski on 06.06.2017.
//  Copyright © 2017 paskowski. All rights reserved.
//

import CoreData

func createNotesContainer(completion: @escaping (NSPersistentContainer) -> ()) {
    let container = NSPersistentContainer(name: "Note")
    container.loadPersistentStores { (_, error) in
        guard error == nil else { fatalError("Failed to load store: \(error)") }
        DispatchQueue.main.async { completion(container) }
    }
}
